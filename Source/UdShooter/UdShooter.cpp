// Copyright Epic Games, Inc. All Rights Reserved.

#include "UdShooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UdShooter, "UdShooter" );
